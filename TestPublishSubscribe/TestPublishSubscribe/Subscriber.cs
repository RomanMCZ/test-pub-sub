﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPublishSubscribe
{
    class Subscriber
    {
        public string SubscriberName { get; private set; }

        public Subscriber(string _subscriberName)
        {
            SubscriberName = _subscriberName;
        }

        // This function subscribe to the event if it is raised by the Publisher
        public void Subscribe(string publisher)
        {
            //https://www.tutorialsteacher.com/csharp/csharp-event
            // register OnNotificationReceived with publisher event
            //p.OnPublish += OnNotificationReceived;  // multicast delegate
            if(Publisher.allPublisher.Exists(x => x.PublisherName == publisher)) 
                Publisher.allPublisher.Find(x => x.PublisherName.Contains(publisher)).OnPublish += OnNotificationReceived; // multicast delegate
        }

        // This function unsubscribe from the event if it is raised by the Publisher
        public void Unsubscribe(string publisher)
        {
            // unregister OnNotificationReceived from publisher
            if (Publisher.allPublisher.Exists(x => x.PublisherName == publisher))
                Publisher.allPublisher.Find(x => x.PublisherName.Contains(publisher)).OnPublish -= OnNotificationReceived; // multicast delegate
        }

        // It get executed when the event published by the Publisher
        protected virtual void OnNotificationReceived(Publisher p, NotificationEvent e)
        {

            Console.WriteLine("Hey " + SubscriberName + ", " + e.NotificationMessage + " - " + p.PublisherName + " at " + e.NotificationDate);
        }
    }
}
