﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestPublishSubscribe
{
    class Publisher
    {
        public static List <Publisher> allPublisher = new List<Publisher>();

        public string PublisherName { get; private set; }
        public int NotificationInterval { get; private set; }

        //** Declare an Event...
        // declare a delegate with any name
        public delegate void Notify(Publisher p, NotificationEvent e);
        // declare a variable of the delegate with event keyword
        public event Notify OnPublish;


        public Publisher(string _publisherName, int _notificationInterval)
        {
            PublisherName = _publisherName;
            NotificationInterval = _notificationInterval;

            allPublisher.Add(this);
        }

        //publish function publishes a Notification Event
        public void Publish(string message)
        {
            /*while (true)
            {

                Thread.Sleep(NotificationInterval); // fire event after certain interval*/

                if (OnPublish != null)
                {
                    NotificationEvent notificationObj = new NotificationEvent(DateTime.Now, message);
                    OnPublish(this, notificationObj);
                }
                //Thread.Yield();
            //}
        }
    }
}
