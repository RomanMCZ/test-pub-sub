﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPublishSubscribe
{
    class NotificationEvent
    {
        public enum StateFLAG
        {
            Info=0,
            Attention = 1,
            ERROR = 2,
        }

        public string NotificationMessage { get; private set; }
        
        public StateFLAG State { get; private set; }

        public DateTime NotificationDate { get; private set; }

        public NotificationEvent(DateTime _dateTime, string _message)
        {
            NotificationDate = _dateTime;
            NotificationMessage = _message;
        }
    }
}
