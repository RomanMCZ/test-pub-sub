﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestPublishSubscribe
{
    class Program
    {
        static void Main(string[] args)
        {
            Publisher stackoverflow = new Publisher("StackOverflow.Com", 3000);
            Publisher facebook = new Publisher("Facebook.com", 1000);


            Subscriber sub1 = new Subscriber("Florin");
            Subscriber sub2 = new Subscriber("Piagio");

            sub1.Subscribe("Facebook.com");
            sub2.Subscribe("StackOverflow.Com");
            
            Transmitter test = new Transmitter();

            sub2.Subscribe("TETRA");
            sub1.Subscribe("TETRA");

            Task task1 = Task.Factory.StartNew(() =>
            {
                while(true)
                {
                    facebook.Publish("Test FACE");
                    Thread.Sleep(1000);
                }
            });
            Task task2 = Task.Factory.StartNew(() => stackoverflow.Publish("STACK CONTROL"));
            Thread.Sleep(Timeout.Infinite);
        }
    }
}
