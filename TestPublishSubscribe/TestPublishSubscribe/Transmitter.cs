﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace TestPublishSubscribe
{
    class Transmitter
    {
        public Transmitter()
        {
            Publisher Transmitter = new Publisher("TETRA", 1000);

            MemoryStream ms = new MemoryStream();
            /*using var writer = new Utf8JsonWriter(ms);*/

            WriteStartObject(ms);
            writeObject(ms,"name", "John Doe");
            writeObject(ms, "occupation", "gardener");
            writeObject(ms, "age", "34");
            WriteEndObject(ms);

            string json = Encoding.UTF8.GetString(ms.ToArray());

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (true)
                { 
                /* run your code here */

                Transmitter.Publish(json);
                Thread.Sleep(1000);
                }
            }).Start();
        }


        void WriteStartObject(MemoryStream stream)
        {
            stream.Write(Encoding.UTF8.GetBytes("{"), 0, 1);
        }

        void WriteEndObject(MemoryStream stream)
        {
            stream.Write(Encoding.UTF8.GetBytes("}"), 0, 1);
        }

        void writeObject(MemoryStream stream, params object[] objects)
        {            
            foreach(object obj in objects)
            {
               /* if (obj.GetType() == typeof(string))
                    switch (obj.GetType())
                    {
                        case typeof(string):*/
                        if(stream.Length >= 2)
                            stream.Write(Encoding.UTF8.GetBytes(","), 0, 1);

                         stream.Write(Encoding.UTF8.GetBytes("\"" + obj.ToString() + "\""), 0, obj.ToString().Length + 2);
                /*          break;
                      default:
                          break;
                  }*/
            }

            Console.WriteLine(Encoding.UTF8.GetString(stream.ToArray()));
                
        }
    }
}
